# -*- coding: utf-8 -*-
# python operate mysql database
import MySQLdb

DATABASE_NAME = ''
HOST          = ''
PORT          = ''
USER_NAME     = ''
PASSWORD      = ''
CHAR_SET      = ''


# 初始化参数
def init():
    global DATABASE_NAME
    global HOST
    global PORT
    global USER_NAME
    global PASSWORD
    global CHAR_SET

    DATABASE_NAME = 'spider'
    HOST = '127.0.0.1'
    PORT = 33060
    USER_NAME = 'homestead'
    PASSWORD = 'secret'
    CHAR_SET = 'utf8'

# 获取数据库连接
def get_conn():
    init()
    try:
        return MySQLdb.connect(
            host      = HOST,
            port      = PORT,
            user      = USER_NAME,
            password  = PASSWORD,
            database  = DATABASE_NAME,
            charset   = CHAR_SET
        )
    except MySQLdb.Error,e:
        print "MySQL Error %d:%s" % (e.args[0],e.args[1])

# 获取cursor
def get_cursor(conn):
    return conn.cursor()


# 关闭连接
def conn_close(conn):
    if conn != None:
        conn.close()


# 关闭cursor
def cursor_close(cursor):
    if cursor != None:
        cursor.close()


# 关闭所有
def close(cursor, conn):
    cursor_close(cursor)
    conn_close(conn)

#执行SQL
# sql    示例 insert into student(id, name, age) values(%s, %s, %s)
# params 示例 ('2', 'Hongten_a', '21')
def handle(sql,params):
    if sql:
        conn = get_conn()
        cursor = get_cursor(conn)
        result = cursor.execute(sql, params)
        conn.commit()
        close(cursor, conn)
        return result
    else:
        print(sql,'不能位空')

def main():
    sql = 'select max(id) as mid from student'
    conn = get_conn()
    cursor = get_cursor(conn)
    result = cursor.execute(sql, ())
    for row in cursor.fetchall():
        print(row)
        for r in row:  # 循环每一条数据
            print(r)
    close(cursor, conn)

if __name__ == '__main__':
    main()