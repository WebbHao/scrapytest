# -*- coding: utf-8 -*-
#引入文件
from scrapy.exceptions import DropItem
import json
import sys

from db import *

reload(sys)
sys.setdefaultencoding( "utf-8" )

class MyPipeline(object):
    def __init__(self):
        #打开文件
        #self.file = open('data.json', 'w',encoding='utf-8')
        self.file = open('data.json', 'w')
    #该方法用于处理数据
    def process_item(self, item, spider):
        #读取item中的数据
        sql = 'insert into sp_course(title,introduction,students,url,image_path,image_url) value (%s,%s,%s,%s,%s,%s)'
        params = (item['title'], item['introduction'], item['student'],item['url'],item['image_path'],item['image_url'])
        #print(params)
        #exit()
        handle(sql, params=params)
        line = json.dumps(dict(item), ensure_ascii=False) + "\n"
        #print(line)
        #写入文件
        self.file.write(line)
        #返回item
        return item
    #该方法在spider被开启时被调用。
    def open_spider(self, spider):
        pass
    #该方法在spider被关闭时被调用。
    def close_spider(self, spider):
        pass